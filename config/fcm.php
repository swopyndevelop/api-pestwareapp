<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAcQGxf9g:APA91bHq4SNzFxi_mJTl6OhhVKWGeKwl7Xd2HyXf_MV-pKs_YUbmtqQPNxtdADXqOyae7D9VyrouxwtPdxCKMPageU5VgNNTBd604ZJGt6ap7UoVMxI_jJ3M8EJzpSfeXKn1Y_M2bT9o'),
        'sender_id' => env('FCM_SENDER_ID', '485359714264'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
